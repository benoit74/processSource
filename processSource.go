package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	if len(os.Args) >= 2 {
		fmt.Println("Usage: processSource yourfile.json")
		os.Exit(-1)
	}

	var jsonFile *os.File
	if len(os.Args) == 1 {
		jsonFile = os.Stdin
	} else {
		fileName := os.Args[1]
		// Open our jsonFile
		var err error
		jsonFile, err = os.Open(fileName)
		// if we os.Open returns an error then handle it
		if err != nil {
			fmt.Println(err)
			os.Exit(-1)
		}
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result map[string]interface{}
	json.Unmarshal([]byte(byteValue), &result)

	cleanedResult := cleanupSources(result)

	cleanedJSON, err := json.MarshalIndent(cleanedResult, "", "  ")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	fmt.Println(string(cleanedJSON))

}

func cleanupSources(inputData interface{}) interface{} {
	if inputDataMap, ok := inputData.(map[string]interface{}); ok {
		if source, ok := inputDataMap["source"]; ok {
			return source
		}
		result := map[string]interface{}{}
		for key, value := range inputDataMap {
			result[key] = cleanupSources(value)
		}
		return result
	}
	return inputData
}
