
# Compiling

```
go build
```

# Usage

## From Stdin

```
processSource < sample.json
```

## From filename

```
processSource sample.json
```


