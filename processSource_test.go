package main

import (
	"reflect"
	"testing"
)

func Test_cleanupSources(t *testing.T) {
	tests := []struct {
		name string
		args interface{}
		want interface{}
	}{
		{
			name: "simple_string",
			args: "string",
			want: "string",
		},
		{
			name: "simple source",
			args: map[string]interface{}{
				"source": "value",
			},
			want: "value",
		},
		{
			name: "nested source",
			args: map[string]interface{}{
				"PRIMARY_DATA": map[string]interface{}{
					"source": "value",
				},
			},
			want: map[string]interface{}{
				"PRIMARY_DATA": "value",
			},
		},
		{
			name: "nested source with other data",
			args: map[string]interface{}{
				"PRIMARY_DATA": map[string]interface{}{
					"source": "value",
				},
				"SOMETHING_ELSE": "othervalue",
			},
			want: map[string]interface{}{
				"PRIMARY_DATA":   "value",
				"SOMETHING_ELSE": "othervalue",
			},
		},
		{
			name: "deeply nested source with other data",
			args: map[string]interface{}{
				"SECONDARAY_DATA": map[string]interface{}{
					"WAHTTEVER": map[string]interface{}{
						"source": "value2",
					},
					"SOMEDATA": map[string]interface{}{
						"hhh": "kkk",
						"ppp": "lll",
					},
				},
				"SOMETHING_ELSE": "othervalue",
			},
			want: map[string]interface{}{
				"SECONDARAY_DATA": map[string]interface{}{
					"WAHTTEVER": "value2",
					"SOMEDATA": map[string]interface{}{
						"hhh": "kkk",
						"ppp": "lll",
					},
				},
				"SOMETHING_ELSE": "othervalue",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := cleanupSources(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("cleanupSources() = %v, want %v", got, tt.want)
			}
		})
	}
}
